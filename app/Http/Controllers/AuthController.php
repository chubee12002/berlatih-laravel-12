<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // untuk tampilin form
	public function index()
	{
		return view('register');
	}
	
	public function register(Request $request)
	{
		$nmDepan = strtoupper($request->first_name);
		$nmBelakang= strtoupper($request->last_name);
		return view('welcome',compact('nmDepan','nmBelakang'));
	}
}
