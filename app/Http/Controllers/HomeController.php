<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // tampilan home index
	public function index()
	{
		return view('home');
	}
}
